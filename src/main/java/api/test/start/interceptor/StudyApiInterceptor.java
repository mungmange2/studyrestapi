package api.test.start.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class StudyApiInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        log.info("{} : 클라이언트의 요청을 컨트롤러에 전달하기 전에 실행 return false 이면 실행되지 않음.", request.getRequestURI());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
        log.info("클라이언트의 요청을 처리한 뒤 호출. 컨트롤러에서 예외가 발생하면 호출되지 않음.");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        log.info("클라이언트 요청 처리 후 클라이언트에 뷰를 통해 응답을 전송한 뒤 실행. 뷰를 생성할때 예외를 발생해도 실행.");
    }
}
