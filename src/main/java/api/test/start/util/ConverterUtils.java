package api.test.start.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConverterUtils {

    @Autowired
    private ObjectMapper objectMapper;

    public String toJsonString(Object o) {
        String value = "{}";
        if (o == null) {
            return value;
        }
        try {
            value = this.objectMapper.writeValueAsString(o);
        } catch (Exception e) {
            return value;
        }
        return value;
    }
}
