package api.test.start.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idx;
    private Long memo_idx;
    private String nick_name;
    private String content;
    private Boolean isDelete;
    private LocalDateTime create_at;
    private LocalDateTime update_at;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="memo_idx", insertable = false, updatable = false) //
    private Memo memo;
}
