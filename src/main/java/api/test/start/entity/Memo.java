package api.test.start.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Data
@Entity
public class Memo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idx;
    private String nick_name;
    private String content;
    private Boolean isDelete;
    private Boolean isComment;
    private LocalDateTime create_at;
    private LocalDateTime update_at;
}
