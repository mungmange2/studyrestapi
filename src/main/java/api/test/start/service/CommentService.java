package api.test.start.service;

import api.test.start.entity.Comment;
import api.test.start.entity.Memo;
import api.test.start.repository.CommentRepository;
import api.test.start.repository.MemoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final MemoRepository memoRepository;

    public void createCommentByTest() {
        Comment comment = new Comment();
        comment.setMemo_idx(3L);
        comment.setContent("댓글이에요~");
        comment.setCreate_at(LocalDateTime.now());
        this.commentRepository.save(comment);

        Memo memo = this.memoRepository.findAllByIdxAndIsDelete(3L, false);
        memo.setIsComment(true);
        comment.setMemo(memo);
        this.memoRepository.save(memo);

    }
}
