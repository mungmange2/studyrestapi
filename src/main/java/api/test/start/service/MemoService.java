package api.test.start.service;

import api.test.start.controller.dto.MemoDto;
import api.test.start.controller.dto.MemoDto.MemoListResponse;
import api.test.start.controller.dto.MemoDto.MemoResponse;
import api.test.start.entity.Memo;
import api.test.start.repository.MemoRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class MemoService {

    private final MemoRepository memoRepository;

    public MemoListResponse readMemoList(Boolean isDelete) {
        MemoListResponse memoListResponse = new MemoListResponse();
        memoListResponse.setMemoList(this.memoRepository.findAllByIsDelete(isDelete));
        return memoListResponse;
    }

    public MemoResponse readMemo(Long idx, Boolean isDelete) {
        MemoResponse memoResponse = new MemoResponse();
        memoResponse.setMemo(this.memoRepository.findAllByIdxAndIsDelete(idx, isDelete));
        return memoResponse;
    }

    public void createMemo(MemoDto.CreateMemo createMemo) {
        Memo memo = new Memo();
        BeanUtils.copyProperties(createMemo, memo);
        memo.setIdx(null);
        this.memoRepository.save(memo);
    }

    public void updateMemo(Long idx, MemoDto.CreateMemo createMemo) {
        Memo memo = this.memoRepository.findAllByIdxAndIsDelete(idx, false);
        if (ObjectUtils.isEmpty(createMemo.getContent())) {
            memo.setContent(memo.getContent());
        }
        if (ObjectUtils.isEmpty(createMemo.getNick_name())) {
            memo.setNick_name(memo.getNick_name());
        }
        memo.setUpdate_at(LocalDateTime.now());
        this.memoRepository.save(memo);
    }

    public void deleteMemo(Long idx) {
        Memo memo = new Memo();
        memo.setIdx(idx);
        memo.setIsDelete(true);
        memo.setUpdate_at(LocalDateTime.now());
        this.memoRepository.save(memo);
    }
}
