package api.test.start.repository;

import api.test.start.entity.Memo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MemoRepository extends CrudRepository<Memo, Long> {
    List<Memo> findAllByIsDelete(Boolean isDelete);
    Memo findAllByIdxAndIsDelete(Long idx, Boolean isDelete);
}
