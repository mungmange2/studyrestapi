package api.test.start.repository;

import api.test.start.entity.Comment;
import api.test.start.entity.Memo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment, Long> {

}
