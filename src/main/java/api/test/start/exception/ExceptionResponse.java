package api.test.start.exception;

import lombok.Data;

@Data
public class ExceptionResponse {

    private int status;
    private String title;
    private String message;
}
