package api.test.start.controller;

import api.test.start.controller.dto.MemoDto;
import api.test.start.controller.dto.MemoDto.MemoListResponse;
import api.test.start.service.MemoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/memo")
@RequiredArgsConstructor
public class MemoController {

    private final MemoService memoService;

    /**
     * 조회
     * */
    @GetMapping(value = "")
    public ResponseEntity<Object> readMemoList() {
        MemoListResponse memoListResponse = this.memoService.readMemoList(false);
        return new ResponseEntity(memoListResponse, HttpStatus.OK);
    }

    /**
     * 상세 조회
     * */
    @GetMapping(value = "/{idx}")
    public ResponseEntity<Object> readMemo(@PathVariable Long idx) {
        MemoDto.MemoResponse memoResponse = this.memoService.readMemo(idx, false);
        return new ResponseEntity(memoResponse, HttpStatus.OK);
    }

    /**
     * 등록
     * */
    @PostMapping(value = "/produce")
    public ResponseEntity createMemo(@RequestBody @Valid MemoDto.CreateMemo createMemo) {
        this.memoService.createMemo(createMemo);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    /**
     * 수정
     * */
    @PutMapping(value = "/{idx}")
    public ResponseEntity updateMemo(@PathVariable Long idx, @ModelAttribute @Valid MemoDto.CreateMemo createMemo) {
        this.memoService.updateMemo(idx, createMemo);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * 삭제
     * */
    @DeleteMapping(value = "/{idx}")
    public ResponseEntity deleteMemo(@PathVariable Long idx) {
        this.memoService.deleteMemo(idx);
        return new ResponseEntity(HttpStatus.OK);
    }
}
