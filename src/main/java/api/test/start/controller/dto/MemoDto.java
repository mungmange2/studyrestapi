package api.test.start.controller.dto;

import api.test.start.entity.Memo;
import lombok.Data;

import java.util.List;

public class MemoDto {

    @Data
    public static class MemoListResponse {
        List<Memo> memoList;
    }

    @Data
    public static class CreateMemo {
        private String nick_name;
        private String content;
    }

    @Data
    public static class MemoResponse {
        Memo memo;
    }
}
