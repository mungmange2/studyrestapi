package api.test.start.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@RestController
public class ExceptionAdvice implements ErrorController {

    @Override
    public String getErrorPath() {
        return null;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity exception(HttpServletRequest request, Exception e) {
        return null;
    }
}
