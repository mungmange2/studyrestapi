DROP TABLE IF EXISTS reviews CASCADE;

CREATE TABLE memo (
     idx           bigint NOT NULL AUTO_INCREMENT, --PK
     nick_name     varchar(50) NOT NULL,           --작성자
     content       varchar(1000) NOT NULL,         --리뷰 내용
     isDelete      bit(1) NOT NULL DEFAULT 0,      --삭제여부
     isComment     bit(1) NOT NULL DEFAULT 0,      --댓글 여부
     create_at     datetime NOT NULL DEFAULT CURRENT_TIMESTAMP(),
     update_at     datetime NULL DEFAULT NULL,
     PRIMARY KEY (idx)
);

CREATE TABLE comment (
      idx           bigint NOT NULL AUTO_INCREMENT, --PK
      memo_idx      bigint NOT NULL, -- memo PK
      nick_name     varchar(50) NOT NULL,           --작성자
      content       varchar(1000) NOT NULL,         --리뷰 내용
      isDelete      bit(1) NOT NULL DEFAULT 0,      --삭제여부
      create_at     datetime NOT NULL DEFAULT CURRENT_TIMESTAMP(),
      update_at     datetime NULL DEFAULT NULL,
      PRIMARY KEY (idx)
);

-- Memo 데이터 생성
INSERT INTO memo(nick_name, content, isDelete) VALUES ('gummy bear', 'I like it!', 0);
INSERT INTO memo(nick_name, content, isDelete) VALUES ('apple angel', 'you are kinds', 0);
INSERT INTO memo(nick_name, content, isDelete) VALUES ('lemon power', 'I want to travel', 0);
INSERT INTO memo(nick_name, content, isDelete) VALUES ('orange prince', 'Hello world', 0);

-- comment 데이터 생성
INSERT INTO comment(memo_idx, nick_name, content, isDelete) VALUES (1, 'gummy bear', '1I like it!', 0);
INSERT INTO comment(memo_idx, nick_name, content, isDelete) VALUES (2, 'gummy bear', '2I like it!', 0);