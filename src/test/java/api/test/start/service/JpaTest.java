package api.test.start.service;

import api.test.start.util.ConverterUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class JpaTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private CommentService commentService;

    @Test
    public void createMemo() throws Exception {
        this.commentService.createCommentByTest();
    }
}
