package api.test.start.controller;

import api.test.start.controller.dto.MemoDto.CreateMemo;
import api.test.start.util.ConverterUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class MemoControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired
    private ConverterUtils converterUtils;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    //@Transactional
    public void createMemo() throws Exception {

        CreateMemo memo = new CreateMemo();
        memo.setContent("TEST");
        memo.setNick_name("mungmang1");

        ResultActions result = mockMvc.perform(post("/memo/produce")
                //.header("X-Payment", null)
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.converterUtils.toJsonString(memo)));

        result.andDo(print());
        result.andExpect(status().isCreated());

        this.readMemoList();
    }

    @Test
    //@Transactional
    public void readMemoList() throws Exception {

        CreateMemo memo = new CreateMemo();
        memo.setContent("TEST");
        memo.setNick_name("mungmang1");

        ResultActions result = mockMvc.perform(get("/memo")
                //.header("X-Payment", null)
                .contentType(MediaType.APPLICATION_JSON));

        result.andDo(print());
        result.andExpect(status().isOk());
    }
}
